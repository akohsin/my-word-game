import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ArrayList<String> listNo1 = new ArrayList();
        ArrayList<String> listNo2 = new ArrayList();

        String path1 = "1player.txt";
        String path2 = "2player.txt";

        readFile(listNo1, path1);
        readFile(listNo2, path2);

        Player player1 = new Player(listNo1);
        Player player2 = new Player(listNo2);

        Scanner sc = new Scanner(System.in);
        boolean roundOf1Player = true;

        while (true) {
            if (roundOf1Player) {
                SingleWord wordOfPlayer1 = player1.giveWord();
                System.out.println();
                System.out.println("First player is throwing a word: " + wordOfPlayer1.getWord());
                if (player2.hasWord(wordOfPlayer1)) {
                    roundOf1Player = false;
                } else {
                    System.out.println("The winner is: Player 1");
                    break;
                }
            } else {
                SingleWord wordOfPlayer2 = player2.giveWord();
                System.out.println();
                System.out.println("Second player is throwing a word " + wordOfPlayer2.getWord());
                if (player1.hasWord(wordOfPlayer2)) {
                    roundOf1Player = true;
                } else {
                    System.out.println("The winner is: Player 2");
                    break;
                }
            }
        }
    }

    private static void readFile(List<String> player1, String path1) {
        try (FileReader readerF = new FileReader(new File(path1));
             BufferedReader reader = new BufferedReader(readerF)) {
            String line = null;
            while (true) {
                line = reader.readLine();
                if (line == null) break;
                line = line.toLowerCase();
                player1.add(line);
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("There's no such file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
