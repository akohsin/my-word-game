import java.util.ArrayList;
import java.util.List;

public class WordsList {
    private ArrayList<SingleWord> wordsInList;

    public WordsList(ArrayList<String> list) {
        ArrayList<SingleWord> wordsInListTmp = new ArrayList<>();
        for (String x : list) {
            wordsInListTmp.add(new SingleWord(x));
        }
        this.wordsInList = wordsInListTmp;
    }

    public List<SingleWord> getWordsInList() {
        return wordsInList;
    }
    public void remove(SingleWord s){
        wordsInList.remove(s);
    }
}
