import java.util.ArrayList;

public class Player {
    WordsList wordsInList;

    public Player(ArrayList<String> x) {
        this.wordsInList = new WordsList(x);
    }

    public boolean hasWord(SingleWord z) {
        boolean has = false;
        for (SingleWord x : wordsInList.getWordsInList()) {
            has=false;
            if (x.getA() == z.getZ()) {
                System.out.println("The opponents response is: "+ x.getWord());
               wordsInList.remove(x);
                return true;
            }
        }

        return false;
    }
    public SingleWord giveWord(){
        SingleWord x = wordsInList.getWordsInList().get(0);
        wordsInList.remove(x);
        return x;
    }
    private void removeWord(SingleWord s){
        wordsInList.remove(s);
    }
}
