public class SingleWord {
    private char a;
    private char z;
    private String word;

    public SingleWord(String word) {
        this.word = word;
        char[] chars = word.toCharArray();
        a=chars[0];
        z=chars[chars.length-1];
    }



    public char getA() {
        return a;
    }

    public char getZ() {
        return z;
    }

    public String getWord() {
        return word;
    }
}
